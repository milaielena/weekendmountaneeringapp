# WeekendMountaneerApp (weekend-mountaneer-app)

## About 
- **Quasar frameworh with Cloud Firestore database.**
- App for planning mountain trips - CRUD, list and table view, trip **sign up**, **carousel** trip preview, **expandable** cards, **Google Maps coordinates**, **OpenWeatherMap API forecast**, EmailJS SDK.

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
