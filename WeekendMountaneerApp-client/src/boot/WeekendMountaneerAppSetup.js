import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBXxrUmjrt_YasR9eDNtDLOH-H5TB8lfS0'
    // libraries: 'places' // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)

    // If you want to set the version, you can do so:
    // v: '3.26',
  },

  // If you intend to programmatically custom event listener code
  // (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
  // instead of going through Vue templates (e.g. `<GmapMap @zoom_changed='someFunc'>`)
  // you might need to turn this on.
  // autobindAllEvents: false,

  // If you want to manually install components, e.g.
  // import {GmapMarker} from 'vue2-google-maps/src/components/marker'
  // Vue.component('GmapMarker', GmapMarker)
  // then set installComponents to 'false'.
  // If you want to automatically install all the components this property must be set to 'true':
  installComponents: true
})

const firebaseConfig = {
  apiKey: 'AIzaSyDtuWADjQ7mJsvAO2nA-99gTJA5mKli428',
  authDomain: 'weekendmountaneeringapp.firebaseapp.com',
  databaseURL: 'https://weekendmountaneeringapp.firebaseio.com',
  projectId: 'weekendmountaneeringapp',
  storageBucket: 'weekendmountaneeringapp.appspot.com',
  messagingSenderId: '47295019004',
  appId: '1:47295019004:web:8c49a2216887f7c24c2479',
  measurementId: 'G-ELDH2WPPWH'
}

firebase.initializeApp(firebaseConfig)

export default ({ Vue }) => {
  Vue.prototype.$auth = firebase.auth()
  Vue.prototype.$db = firebase.firestore()
  Vue.prototype.$storage = firebase.storage()
}
